// Copyright (C) 2008  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QClipboard>
#include <QSplitter>
#include <QStackedWidget>
#include <QUndoGroup>
#include <QAction>
#include "diagramview.h"
#include "diagramdocument.h"

class QPrinter;

class MainWindow: public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(const QString &fileName = QString());
	~MainWindow();

	QIcon loadIcon(const QString &name);

public slots:
	void newModel(DiagramDocument *newModel = 0);

	void updateMode(DiagramDocument::Mode mode);
	void switchModeSelect();
	void switchModeAddTable();
	void switchModeAddRelation();
	void setDiagramNotation(QAction *action);

	void updateSelection();
	void deleteSelectedItems();

	void open();
	bool save();
	bool maybeSave();
	bool saveAs();
	void exportDocument();
	void print();
	void pageSetup();
	void printPreview();
	void printPreview(QPrinter *);
	void setViewScale(const QString &scale);
	void about();

	void loadFile(const QString &fileName);
	void saveFile(const QString &fileName);

	void addRecentFile(const QString &fileName);
	void openRecentFile();
	void updateRecentFileActions();

	void updateWindowTitle();

	void cut();
	void copy();
	void paste();
	void updateClipboard(QClipboard::Mode mode);

    void closeDocument();
	void closeAll();

	void showGrid(bool);

protected:
	void setupUi();
	void setupActions();
	void setupToolBar();
	void setupMenuBar();

	void closeEvent(QCloseEvent *event);
	void resizeEvent(QResizeEvent *event);
	void moveEvent(QMoveEvent *event);
	void showEvent(QShowEvent *event);

	void saveWindowState();
	void restoreWindowState();

    static QList<MainWindow *> mainWindows();

private:
	class MainWindowPrivate;
	MainWindowPrivate *const d;

    void initPrinter();

	DiagramView *m_view;
	DiagramDocument *m_model;

	QAction *m_actionUndo, *m_actionRedo;
	QAction *m_actionSwitchMode[3];
	QAction *m_actionNew;
	QAction *m_actionOpen;
	QAction *m_actionSave;
	QAction *m_actionSaveAs;
	QAction *m_actionExportPNG;

	QAction *m_actionClose;
	QAction *m_actionQuit;

	QAction *m_actionShowGrid;

	QAction *m_actionCut;
	QAction *m_actionCopy;
	QAction *m_actionPaste;
	QAction *m_actionDelete;
	QAction *m_actionAbout;

	QPoint m_lastPos;
	QSize m_lastSize;

	enum { MaxRecentFiles = 5 };
	QAction *m_actionRecentFilesSeparator;
	QAction *m_actionRecentFile[MaxRecentFiles];
};

#endif
