// Copyright (C) 2008  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#ifndef DATABASEMODELVIEW_H
#define DATABASEMODELVIEW_H

#include <QGraphicsView>
class DiagramDocument;
class QTimer;

class DiagramView : public QGraphicsView
{
	Q_OBJECT

public:
	DiagramView(QWidget *parent = 0);

	DiagramDocument *document() const;

	void setScene(QGraphicsScene *scene);

protected:
	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);

	void drawBackground(QPainter *painter, const QRectF &rect);
	void drawGrid(QPainter *painter, const QRectF &rect);

protected slots:
	void updateSceneRect2(const QRectF &rect);
	void doAutoScroll();

private:
    void startAutoScroll();
    void stopAutoScroll();
    bool isAutoScrolling() const;
    bool shouldAutoScroll(const QPoint &pos) const;

	bool m_handScrolling;
	QPoint m_handScrollingOrigin;
	QCursor m_savedCursor;
	int m_handScrollingValueX;
	int m_handScrollingValueY;
	QTimer *m_autoScrollTimer;
	int m_autoScrollCount;
	int m_autoScrollMargin;
};

#endif
