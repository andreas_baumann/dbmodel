// Copyright (C) 2009  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#include <QStringList>
#include "exporter.h"
#include "pdfexporter.h"
#include "pngexporter.h"
#include "svgexporter.h"
#include "exporterlist.h"

ExporterList::ExporterList()
{
    append(new PngExporter());
    append(new PdfExporter());
    append(new SvgExporter());
}

ExporterList::~ExporterList()
{
    qDeleteAll(*this);
}

QString
ExporterList::filters() const
{
    QStringList filters;
    for (int i = 0; i < count(); i++) {
        filters.append(at(i)->filter());
    }
    return filters.join(";;");
}

Exporter *
ExporterList::lookup(QString *fileName, const QString &selectedFilter) const
{
    int index = fileName->lastIndexOf('.');
    if (index == -1 && !selectedFilter.isEmpty()) {
        // Add missing extension based on the selected filter
        for (int i = 0; i < count(); i++) {
            Exporter *exporter = at(i);
            if (exporter->filter() == selectedFilter) {
                fileName->append(exporter->extension());
                break;
            }
        }
    }

    for (int i = 0; i < count(); i++) {
        Exporter *exporter = at(i);
        if (fileName->endsWith(exporter->extension(), Qt::CaseInsensitive)) {
            return exporter;
        }
    }

    return NULL;
}
