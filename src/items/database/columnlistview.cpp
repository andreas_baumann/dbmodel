// Copyright (C) 2008  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#include "columnlist.h"
#include "columnlistmodel.h"
#include "columnlistview.h"
#include "databasecommands.h"
#include "diagramdocument.h"
#include "utils/comboboxdelegate.h"

ColumnListView::ColumnListView(QWidget *parent)
	: QTreeView(parent)
{
    setRootIsDecorated(false);
    setItemsExpandable(false);
    setExpandsOnDoubleClick(false);
	setModel(new ColumnListModel(this));
	ComboBoxDelegate *delegate = new ComboBoxDelegate(this);
	delegate->setEditable(true);
	// FIXME postgresql specific, not a complete list
	delegate->setOptions(QStringList()
		<< "BIGINT"
		<< "BIGSERIAL"
		<< "BIT(n)"
		<< "VARBIT(n)"
		<< "BOOLEAN"
		<< "VARCHAR(n)"
		<< "CHAR(n)"
		<< "DATE"
		<< "INTEGER"
		<< "MONEY"
		<< "NUMERIC(p,s)"
		<< "REAL"
		<< "SMALLINT"
		<< "SERIAL"
		<< "TEXT"
		<< "TIME"
		<< "TIME WITH TIME ZONE"
		<< "TIMESTAMP"
		<< "TIMESTAMP WITH TIME ZONE"
		<< "UUID"
		<< "XML"
		);
	setItemDelegateForColumn(ColumnListModel::TypeColumn, delegate);
}

void
ColumnListView::setColumnList(ColumnList *columnList)
{
	columnListModel()->setColumnList(columnList);
}

void
ColumnListView::addColumn()
{
	AddColumnCommand *command = new AddColumnCommand(columnList());
	columnList()->table()->document()->undoStack()->push(command);
	QModelIndex index = columnListModel()->indexFromRow(command->index());
	setCurrentIndex(index);
	edit(index);
}

void
ColumnListView::removeColumn()
{
	QList<int> indexes = selectedColumns();
	if (indexes.size() == 1) {
		columnList()->table()->document()->undoStack()->push(
			new RemoveColumnCommand(columnList(), indexes[0]));
	}
}

void
ColumnListView::moveColumnDown()
{
	QList<int> indexes = selectedColumns();
	if (indexes.size() == 1) {
		int i = indexes[0];
		if (i + 1 < columnList()->columnCount()) {
			columnList()->table()->document()->undoStack()->push(
				new SwapColumnsCommand(columnList(), i, i + 1));
			setCurrentIndex(columnListModel()->indexFromRow(i + 1));
		}
	}
}

void
ColumnListView::moveColumnUp()
{
	QList<int> indexes = selectedColumns();
	if (indexes.size() == 1) {
		int i = indexes[0];
		if (i > 0) {
			columnList()->table()->document()->undoStack()->push(
				new SwapColumnsCommand(columnList(), i, i - 1));
			setCurrentIndex(columnListModel()->indexFromRow(i - 1));
		}
	}
}

QModelIndexList
ColumnListView::selectedIndexes() const
{
	QModelIndexList indexes;
	foreach (QModelIndex index, selectionModel()->selectedIndexes()) {
		if (index.column() == 0) {
			indexes << index;
		}
	}
	return indexes;
}

QList<int>
ColumnListView::selectedColumns() const
{
	QList<int> columns;
	foreach (QModelIndex index, selectedIndexes()) {
		columns << index.row();
	}
	return columns;
}
