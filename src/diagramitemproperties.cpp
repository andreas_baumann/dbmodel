// Copyright (C) 2008  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#include "diagramitemproperties.h"

class DiagramItemProperties::DiagramItemPropertiesPrivate
{
public:
	DiagramItemPropertiesPrivate()
		: currentItem(0)
		{}
	DiagramItem *currentItem;
};

DiagramItemProperties::DiagramItemProperties(QWidget *parent)
	: QTabWidget(parent), d(new DiagramItemPropertiesPrivate)
{
}

DiagramItemProperties::~DiagramItemProperties()
{
	delete d;
}

int
DiagramItemProperties::addPage(const QString &label, QWidget *page)
{
	return addTab(page, label);
}

DiagramItem *
DiagramItemProperties::currentItem() const
{
	return d->currentItem;
}

void
DiagramItemProperties::setCurrentItem(DiagramItem *item)
{
	DiagramItem *oldItem = d->currentItem;
	d->currentItem = item;
	switchCurrentItem(oldItem, item);
}

void
DiagramItemProperties::switchCurrentItem(DiagramItem *oldItem, DiagramItem *newItem)
{
	Q_UNUSED(oldItem)
	Q_UNUSED(newItem)
}
