// Copyright (C) 2009  Lukas Lalinsky
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#ifndef DIAGRAM_LINELAYOUTER_H
#define	DIAGRAM_LINELAYOUTER_H

#include <QObject>
#include <QTimer>

class Line;

namespace Diagram {

class LineLayouter : public QObject
{
	Q_OBJECT

public:
	LineLayouter();
	~LineLayouter();

	void updateLine(Line *line);
	void addLine(Line *line);

protected slots:
	void doUpdate();

private:
	QTimer *m_updateTimer;
};

}

#endif
