DEPENDPATH += $$PWD

include(colorpicker/colorpicker.pri)
include(iconloader/iconloader.pri)

HEADERS += \
	factory.h \
	range.h \
	singelton.h \
	comboboxdelegate.h

SOURCES += \
	comboboxdelegate.cpp
