<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR" sourcelanguage="en">
<context>
    <name>ColorPickerPopup</name>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="897"/>
        <source>Custom</source>
        <translation>Personnaliser</translation>
    </message>
</context>
<context>
    <name>ColumnListModel</name>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="177"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="179"/>
        <source>Data Type</source>
        <translation>Type de donnée</translation>
    </message>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="181"/>
        <source>Req&apos;d</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="183"/>
        <source>PK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/items/database/columnlistmodel.cpp" line="185"/>
        <source>Notes</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DatabaseRelationshipForm</name>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="16"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="28"/>
        <source>Automatic</source>
        <translation>Automatique</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="37"/>
        <source>Cardinality:</source>
        <translation>Cardinalité :</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="46"/>
        <source>1:1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="53"/>
        <source>1:N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="60"/>
        <source>M:N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="82"/>
        <source>Modality:</source>
        <translation>Modalité :</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="91"/>
        <source>Parent is optional</source>
        <translation>Parent est optionel</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="98"/>
        <source>Child is optional</source>
        <translation>Enfant est optionel</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="120"/>
        <source>Columns:</source>
        <translation>Colonnes :</translation>
    </message>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.ui" line="139"/>
        <source>references</source>
        <translation>Références :</translation>
    </message>
</context>
<context>
    <name>DatabaseRelationshipProperties</name>
    <message>
        <location filename="../src/items/database/databaserelationshipproperties.cpp" line="53"/>
        <source>&amp;Relationship</source>
        <translation>&amp;Relation</translation>
    </message>
</context>
<context>
    <name>DatabaseTableProperties</name>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="50"/>
        <source>&amp;Table</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="51"/>
        <source>&amp;Columns</source>
        <translation>&amp;Colonnes</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="68"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="72"/>
        <source>White</source>
        <translation>Blanc</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="73"/>
        <location filename="../src/items/database/databasetableproperties.cpp" line="74"/>
        <location filename="../src/items/database/databasetableproperties.cpp" line="75"/>
        <source>Gray</source>
        <translation>Gris</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="76"/>
        <source>Red</source>
        <translation>Rouge</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="77"/>
        <source>Brown</source>
        <translation>Marron</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="78"/>
        <source>Pink</source>
        <translation>Rose</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="79"/>
        <source>Yellow</source>
        <translation>Jaune</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="80"/>
        <source>Green</source>
        <translation>Vert</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="81"/>
        <source>Blue</source>
        <translation>Bleu</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="83"/>
        <source>Color:</source>
        <translation>Couleur :</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="106"/>
        <source>&amp;Add</source>
        <translation>&amp;Ajouter</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="107"/>
        <source>&amp;Remove</source>
        <translation>&amp;Enlever</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="108"/>
        <source>Move &amp;Up</source>
        <translation>En Ha&amp;ut</translation>
    </message>
    <message>
        <location filename="../src/items/database/databasetableproperties.cpp" line="109"/>
        <source>Move &amp;Down</source>
        <translation>En &amp;Bas</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="166"/>
        <source>&amp;Properties</source>
        <translation>&amp;Propriétés</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="204"/>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="207"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="211"/>
        <source>&amp;Open...</source>
        <translation>&amp;Ouvrir...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="213"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="217"/>
        <source>&amp;Save</source>
        <translation>&amp;Sauver</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="219"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="224"/>
        <source>Save &amp;As...</source>
        <translation>S&amp;auver comme...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="233"/>
        <source>E&amp;xport...</source>
        <translation>E&amp;xporter...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="237"/>
        <source>Page Set&amp;up...</source>
        <translation>&amp;Mise en page... </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="241"/>
        <source>&amp;Print...</source>
        <translation>&amp;Imprimer...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="243"/>
        <source>Ctrl+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="247"/>
        <source>Print Previe&amp;w...</source>
        <translation>Pré&amp;visualiser...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="249"/>
        <source>Ctrl+Shift+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="259"/>
        <source>Select</source>
        <translation>Sélectionner</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="265"/>
        <source>Add new table</source>
        <translation>Ajouter une nouvelle table</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="270"/>
        <source>Add new relation</source>
        <translation>Ajouter une nouvelle relation</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="274"/>
        <source>&amp;Undo</source>
        <translation>&amp;Defaire</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="275"/>
        <source>Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="277"/>
        <source>Re&amp;do</source>
        <translation>&amp;Refaire</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="278"/>
        <source>Ctrl+Shift+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="286"/>
        <source>Cu&amp;t</source>
        <translation>C&amp;ouper</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="288"/>
        <source>Ctrl+X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="292"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copier</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="294"/>
        <source>Ctrl+C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="298"/>
        <source>&amp;Paste</source>
        <translation>Co&amp;ller</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="300"/>
        <source>Ctrl+V</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="304"/>
        <source>&amp;Delete</source>
        <translation>&amp;Effacer</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="306"/>
        <source>Del</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="310"/>
        <source>&amp;About...</source>
        <translation>&amp;A propos...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="314"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="316"/>
        <source>Ctrl+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="320"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="322"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="326"/>
        <source>Show &amp;Grid</source>
        <translation>Afficher la &amp;Grille</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="330"/>
        <source>&amp;Notation</source>
        <translation>&amp;Notation</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="333"/>
        <source>&amp;Relational</source>
        <translation>&amp;Relationel</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="337"/>
        <source>&amp;Crow&apos;s Foot</source>
        <translation>Pied de &amp;Crow</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="354"/>
        <location filename="../src/mainwindow.cpp" line="403"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="369"/>
        <source>&amp;Mode</source>
        <translation>&amp;Mode</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>50%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>70%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>85%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>100%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>125%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>150%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="385"/>
        <location filename="../src/mainwindow.cpp" line="433"/>
        <source>&amp;View</source>
        <translation>&amp;Vue</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="423"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editer</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="438"/>
        <source>&amp;Diagram</source>
        <translation>&amp;Diagramme</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="442"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="480"/>
        <source>The document has been modified.
Do you want to save your changes?</source>
        <translation>Le document a été modifié. Voulez vous sauver les changements ?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="461"/>
        <location filename="../src/mainwindow.cpp" line="528"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="461"/>
        <location filename="../src/mainwindow.cpp" line="528"/>
        <source>Unknown format.</source>
        <translation>Format inconnue.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="741"/>
        <source>&amp;%1. %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="798"/>
        <source>About</source>
        <translation>A propos</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="798"/>
        <source>&lt;p&gt;
&lt;b&gt;Database Modeller </source>
        <translation>Concepteur de Base de données</translation>
    </message>
    <message>
        <source>&lt;p&gt;
&lt;b&gt;Database Modeller&lt;/b&gt;&lt;br /&gt;
&lt;a href=&quot;http://oxygene.sk/lukas/dbmodel/&quot;&gt;http://oxygene.sk/lukas/dbmodel/&lt;/a&gt;&lt;br /&gt;
Copyright (C) 2008 Lukas Lalinsky
&lt;/p&gt;
</source>
        <translation type="obsolete">&lt;p&gt;&lt;b&gt;Database Modeller&lt;/b&gt;&lt;br /&gt;&lt;a href=&quot;http://oxygene.sk/lukas/dbmodel/&quot;&gt;http://oxygene.sk/lukas/dbmodel/&lt;/a&gt;&lt;br /&gt;Copyright (C) 2008 Lukáš Lalinský&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="813"/>
        <source>Untitled</source>
        <translation>Sans titre</translation>
    </message>
</context>
<context>
    <name>QtColorPicker</name>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="279"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="405"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="508"/>
        <source>Black</source>
        <translation>Noir</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="406"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="509"/>
        <source>White</source>
        <translation>Blanc</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="407"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="510"/>
        <source>Red</source>
        <translation>Rouge</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="408"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="511"/>
        <source>Dark red</source>
        <translation>Rouge foncé</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="409"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="512"/>
        <source>Green</source>
        <translation>Vert</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="410"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="513"/>
        <source>Dark green</source>
        <translation>Vert foncé</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="411"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="514"/>
        <source>Blue</source>
        <translation>Bleu</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="412"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="515"/>
        <source>Dark blue</source>
        <translation>Bleu foncé</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="413"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="516"/>
        <source>Cyan</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="414"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="517"/>
        <source>Dark cyan</source>
        <translation>Cyan foncé</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="415"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="518"/>
        <source>Magenta</source>
        <translation>Violet</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="416"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="519"/>
        <source>Dark magenta</source>
        <translation>Violet foncé</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="417"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="520"/>
        <source>Yellow</source>
        <translation>Jaune</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="418"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="521"/>
        <source>Dark yellow</source>
        <translation>Jaune foncé</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="419"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="522"/>
        <source>Gray</source>
        <translation>Gris</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="420"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="523"/>
        <source>Dark gray</source>
        <translation>Gris foncé</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="421"/>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="524"/>
        <source>Light gray</source>
        <translation>Gris clair</translation>
    </message>
    <message>
        <location filename="../src/utils/colorpicker/qtcolorpicker.cpp" line="439"/>
        <source>Custom</source>
        <translation>Personnaliser</translation>
    </message>
</context>
</TS>
